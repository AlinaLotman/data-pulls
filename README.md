# Data Pulls

This Folder Contains All reoccuring quarterly/Monthly Data pulls.
They are segmented by Client.
New Reoccuring requests should be added to this repository.

+----------+----------------+-------------+------------+----------+-----------------------+------------------------------------------------------------------+
|  Client  |    PullType    | Granularity | TimePeriod | Projects |      Description      |                           Special Note                           |
+----------+----------------+-------------+------------+----------+-----------------------+------------------------------------------------------------------+
| Sitel    | Profile Rec    | JP          | Monthly    | W@H      |                       |                                                                  |
| Sitel    | Profile Rec    | JP          | Monthly    | B&M      |                       |                                                                  |
| Sitel    | Testing Time   | Assessment  | Monthly    | W@H      |                       |                                                                  |
| Sitel    | Testing Time   | Assessment  | Monthly    | B&M      |                       |                                                                  |
| LM       | Profile Rec    | JP          | Quarterly  | All      |                       |                                                                  |
| LM       | Testing Time   | Assessment  | Quarterly  | All      |                       |                                                                  |
| LM       | Module(survey) | Responses   | Quarterly  | All      | Module 6352           |                                                                  |
| Cox      | Module(survey) | PAID        | Quarterly  | All      | Module 5678           | demographics hence PAID instead of Job Profile, pivot as columns |
| Cox      | Testing Time   | Assessment  | Quarterly  | All      |                       | include group                                                    |
| Cox      | Profile Rec    | JP          | Quarterly  | All      |                       |                                                                  |
| Cox      | Module(survey) | PAID        | Quarterly  | All      | Module 5799           |                                                                  |
| MP       | Module(survey) | Responses   | Quarterly  | All      | Module 3742 (english) |                                                                  |
| MP       | Module(survey) | Responses   | Quarterly  | All      | Module 3790 (french)  |                                                                  |
| MP       | Module(survey) | Responses   | Quarterly  | All      | Module 3781 (spanish) |                                                                  |
| MP       | Module(survey) | PAID        | Quarterly  | All      | Module 3739 (english) | Filter: Group = Lansing, pivot                                   |
| MP       | Module(survey) | PAID        | Quarterly  | All      | Module 3778 (spanish) | Filter: Group = Lansing, pivot                                   |
| MP       | Profile Rec    | JP          | Quarterly  | All      |                       |                                                                  |
| MP       | Testing Time   | Assessment  | Quarterly  | All      |                       |                                                                  |
| Hartford | Profile Rec    | JP          | Quarterly  | All      |                       |                                                                  |
| Hartford | Testing Time   | Assessment  | Quarterly  | All      |                       | exclude incomplete                                               |
| Hartford | Module(survey) | Responses   | Quarterly  | All      | Module 6106           |                                                                  |
+----------+----------------+-------------+------------+----------+-----------------------+------------------------------------------------------------------+