/*
	CORE-6318 - Q4 2017 Cox Data Pulls
	Parameters Passed - 
	1. Client ID = 10289
	2. Date Range - 1-Oct-2017 to 31-Dec-2017
	3. Module Id = 5799 (granularity, unique paid per row) -- No Pivoting
	4. Module Id = 5678 (granularity, unique paid per row) -- Needs Pivoting
*/
Declare @clientID INT 
Declare @startdate Date
Declare @enddate Date
Declare @ModuleID INT

Set @clientID=10289
Set @startdate = '2017-10-01'
Set @enddate = '2017-12-31'
Set @ModuleID = 5678 

select distinct * from (
SELECT distinct
pa.ParticipantApplicationID,
p.ParticipantID,
'"' + FirstName + '"' AS FirstName,
'"' + LastName + '"' AS LastName,
Email,
EmpID
,c.ClientID,c.ClientLongName,
ProjectName,
PG.ProjectGroupName,
pawsm.JobWorkflowStepID,

pawsm.ModuleID,
ModuleName,
ja.JobApplicationID,
ja.JobApplicationName,

ControlName,
ControlItemID,
'"'+ ControlOptionItem + '"' AS ControlOptionItem,
ControlOptionValue=Replace(Replace(ControlOptionValue, CHAR(13),''),char(10),''),
QuestionID,
pawsm.Status,
pawsm.Notes,
pa.CreatedDate  PA_CreatedDate,
pa.ModifiedDate PA_ModifiedDate,
pa.CreatedBy PA_CreatedBy,
pa.ModifiedBy PA_ModifiedBy,

pawsm.CreatedDate PAWSM_CreatedDate,
pawsm.ModifiedDate PAWSM_ModifiedDate,
pawsm.CreatedBy PAWSM_CreatedBy,
pawsm.ModifiedBy PAWSM_ModifiedBy


FROM Participant as p with(NOLOCK)
Inner Join dbo.Client c WITH(NOLOCK) ON c.ClientID=p.ClientID
INNER JOIN ParticipantApplication as pa with(NOLOCK) ON p.ParticipantID=pa.ParticipantID -- pawsm.ParticipantApplicationID = pa.ParticipantApplicationID

INNER JOIN Project as prj with(NOLOCK) ON prj.ProjectID = p.ProjectID
LEFT JOIN ProjectGroup PG with(NOLOCK) on prj.ProjectID=pg.ProjectID and p.ProjectGroupID=pg.ProjectGroupID
LEFT JOIN JobApplication as ja with(NOLOCK) ON ja.JobApplicationID = pa.JobApplicationID

LEFT JOIN dbo.ParticipantJobApplicationWorkflowStep paws with(NOLOCK) on paws.ParticipantApplicationID=pa.ParticipantApplicationID
and paws.JobApplicationID=pa.JobApplicationID 

LEFT JOIN ParticipantJobApplicationWorkflowStepModule as pawsm with(NOLOCK) on 
pawsm.ParticipantID=P.ParticipantID 
and pawsm.ParticipantApplicationID=pa.ParticipantApplicationID
and pawsm.JobApplicationID=ja.JobApplicationID
and paws.JobWorkflowStepID = pawsm.JobWorkflowStepID and paws.ModuleID=pawsm.ModuleID

WHERE p.ClientID=@clientID
and pawsm.ModuleID=@ModuleID
and CAST(paws.CreatedDate as date) >= @startdate and CAST(paws.CreatedDate as date) <=@enddate 
and paws.Status=3


UNION 

SELECT distinct
pa.ParticipantApplicationID,
p.ParticipantID,
'"' + FirstName + '"' AS FirstName,
'"' + LastName + '"' AS LastName,
Email,
EmpID
,c.ClientID,c.ClientLongName,
ProjectName,
PG.ProjectGroupName,
pawsm.JobWorkflowStepID,

pawsm.ModuleID,
ModuleName,
ja.JobApplicationID,
ja.JobApplicationName,

ControlName,
ControlItemID,
'"'+ ControlOptionItem + '"' AS ControlOptionItem,
ControlOptionValue=Replace(Replace(ControlOptionValue, CHAR(13),''),char(10),''),
QuestionID,
pawsm.Status,
pawsm.Notes,
pa.CreatedDate  PA_CreatedDate,
pa.ModifiedDate PA_ModifiedDate,
pa.CreatedBy PA_CreatedBy,
pa.ModifiedBy PA_ModifiedBy,

pawsm.CreatedDate PAWSM_CreatedDate,
pawsm.ModifiedDate PAWSM_ModifiedDate,
pawsm.CreatedBy PAWSM_CreatedBy,
pawsm.ModifiedBy PAWSM_ModifiedBy


FROM MIGArchive.dbo.Participant as p with(NOLOCK)
Inner Join dbo.Client c WITH(NOLOCK) ON c.ClientID=p.ClientID
INNER JOIN MIGArchive.dbo.ParticipantApplication as pa with(NOLOCK) ON p.ParticipantID=pa.ParticipantID -- pawsm.ParticipantApplicationID = pa.ParticipantApplicationID
INNER JOIN Project as prj with(NOLOCK) ON prj.ProjectID = p.ProjectID
LEFT JOIN ProjectGroup PG with(NOLOCK) on prj.ProjectID=pg.ProjectID and p.ProjectGroupID=pg.ProjectGroupID
LEFT JOIN MIGArchive.dbo.JobApplication as ja with(NOLOCK) ON ja.JobApplicationID = pa.JobApplicationID

LEFT JOIN MIGArchive.dbo.ParticipantJobApplicationWorkflowStep as paws with(NOLOCK) on paws.ParticipantApplicationID=pa.ParticipantApplicationID
and paws.JobApplicationID=pa.JobApplicationID

LEFT JOIN MIGArchive.dbo.ParticipantJobApplicationWorkflowStepModule as pawsm with(NOLOCK) on pawsm.ParticipantID=P.ParticipantID and pawsm.ParticipantApplicationID=pa.ParticipantApplicationID
and pawsm.JobApplicationID=ja.JobApplicationID
and paws.JobWorkflowStepID = pawsm.JobWorkflowStepID and paws.ModuleID=pawsm.ModuleID

WHERE p.ClientID=@clientID
and pawsm.ModuleID=@ModuleID
and CAST(paws.CreatedDate as date) >= @startdate and CAST(paws.CreatedDate as date) <=@enddate 
and paws.Status=3
) A
Order by A.PA_CreatedDate