/*
	CORE-6319 - Q4 2017 MP Data Pulls
	Parameters Passed - 
	1. Client ID = 10179
	2. Date Range - 1-Oct-2017 to 31-Dec-2017
*/
select distinct
ed.ParticipantApplicationID,
ed.ParticipantID,
ed.FirstName,ed.LastName,
ed.Email,
ed.JobApplicationID,
ed.JobApplicationName,
ed.ClientID,
ed.ClientLongName,
ed.ProjectID,
ed.ProjectName,
AF.FormID,
AF.FormName,
ed.ProjectGroupID,
ed.ProjectGroupName,
ed.CreatedDate,
PAD.AssessmentID,
A.AssessmentName,
PAD.AssessmentStartTime,
PAD.AssessmentEndTime,
PAD.AssessmentDuration AssessmentDuration_fromtable,
CASE WHEN DATEDIFF(HH,PAD.AssessmentStartTime,PAD.AssessmentEndTime)/24 > 0 THEN
CONVERT(VARCHAR(12), CAST(PAD.AssessmentDuration AS INT) /60/60/24) ELSE 0 END AS AssessmentDurationDays,
CONVERT(VARCHAR(12),CAST(PAD.AssessmentDuration AS INT) /60/60 % 24) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) /60 % 60) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) % 60) AS AssessmentDuration
From [dbo].[vwParticipantProgressExportDetails] As ED
Left Outer Join ParticipantJob PJ
On PJ.ParticipantApplicationId = ED.participantapplicationid And PJ.JobApplicationId = ED.JobApplicationId
Left Outer Join [MigArchive].[dbo].ParticipantJob PJa On
PJa.ParticipantApplicationId = ED.participantapplicationid And PJa.JobApplicationId = ED.JobApplicationId
Inner Join Job J On (J.JobId = PJ.JobId Or J.JobId = PJa.JobId)
left join ParticipantApplicationAssessmentDuration PAD
on (ed.ParticipantApplicationID=PAD.ParticipantApplicationID and
ed.ParticipantID=PAD.ParticipantID AND ED.JobApplicationID=PAD.JobApplicationID)
left join Assessment A on (PAD.AssessmentID=A.AssessmentID)
left join AssessmentForm AF on(AF.AssessmentID=PAD.AssessmentID AND PAD.FormID=AF.FormID) 
where ed.ClientID = 10179 --ManPower
and CAST(ed.CreatedDate as date) between '2017-10-01' and '2017-12-31'
AND (CONVERT(VARCHAR(12),CAST(PAD.AssessmentDuration AS INT) /60/60 % 24) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) /60 % 60) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) % 60)) IS NOT NULL
--order by ed.createdDate

UNION

select distinct
ed.ParticipantApplicationID,
ed.ParticipantID,
ed.FirstName,ed.LastName,
ed.Email,
ed.JobApplicationID,
ed.JobApplicationName,
ed.ClientID,
ed.ClientLongName,
ed.ProjectID,
ed.ProjectName,
AF.FormID,
AF.FormName,
ed.ProjectGroupID,
ed.ProjectGroupName,
ed.CreatedDate,
PAD.AssessmentID,
A.AssessmentName,
PAD.AssessmentStartTime,
PAD.AssessmentEndTime,
PAD.AssessmentDuration AssessmentDuration_fromtable,
CASE WHEN DATEDIFF(HH,PAD.AssessmentStartTime,PAD.AssessmentEndTime)/24 > 0 THEN
CONVERT(VARCHAR(12), CAST(PAD.AssessmentDuration AS INT) /60/60/24) ELSE 0 END AS AssessmentDurationDays,
CONVERT(VARCHAR(12),CAST(PAD.AssessmentDuration AS INT) /60/60 % 24) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) /60 % 60) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) % 60) AS AssessmentDuration
From MIGArchive.dbo.vwParticipantApplicationDetails0913 As ED
Left Outer Join ParticipantJob PJ
On PJ.ParticipantApplicationId = ED.participantapplicationid And PJ.JobApplicationId = ED.JobApplicationId
Left Outer Join [MigArchive].[dbo].ParticipantJob PJa On
PJa.ParticipantApplicationId = ED.participantapplicationid And PJa.JobApplicationId = ED.JobApplicationId
Inner Join Job J On (J.JobId = PJ.JobId Or J.JobId = PJa.JobId)
left join ParticipantApplicationAssessmentDuration PAD
on (ed.ParticipantApplicationID=PAD.ParticipantApplicationID and
ed.ParticipantID=PAD.ParticipantID AND ED.JobApplicationID=PAD.JobApplicationID)
left join Assessment A on (PAD.AssessmentID=A.AssessmentID)
left join AssessmentForm AF on(AF.AssessmentID=PAD.AssessmentID AND PAD.FormID=AF.FormID) 
where ed.ClientID =  10179 --ManPower
and CAST(ed.CreatedDate as date) between '2017-10-01' and '2017-12-31'
AND (CONVERT(VARCHAR(12),CAST(PAD.AssessmentDuration AS INT) /60/60 % 24) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) /60 % 60) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) % 60)) IS NOT NULL
--order by ed.createdDate