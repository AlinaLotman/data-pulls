----- This script is used to extract testing time for Apple Client
----- All assessment duration are stored in ParticipantApplicationAssessmentDuration table in MIG except for 'React Sim' that is stored in 'SimulationDuration' in Simulation DB
----- This script UNIONs the data between the 2 tables

select distinct
ed.ParticipantApplicationID,
ed.ParticipantID,
ed.FirstName,ed.LastName,
ed.Email,
ed.JobApplicationID,
ed.JobApplicationName,
ed.ClientID,
ed.ClientLongName,
ed.ProjectID,
ed.ProjectName,
AF.FormID,
AF.FormName,
ed.ProjectGroupID,
ed.ProjectGroupName,
ed.CreatedDate,
PAD.AssessmentID,
A.AssessmentName,
PAD.AssessmentStartTime,
PAD.AssessmentEndTime,
PAD.AssessmentDuration AssessmentDuration_fromtable,
CASE 
WHEN DATEDIFF(HH,PAD.AssessmentStartTime,PAD.AssessmentEndTime)/24 > 0 THEN
CONVERT(VARCHAR(12), CAST(PAD.AssessmentDuration AS INT) /60/60/24) ELSE 0 END AS AssessmentDurationDays,
CONVERT(VARCHAR(12),CAST(PAD.AssessmentDuration AS INT) /60/60 % 24) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) /60 % 60) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) % 60) AS AssessmentDuration
From [dbo].[vwParticipantProgressExportDetails] As ED
Left Outer Join ParticipantJob  PJ with(nolock)
On PJ.ParticipantApplicationId = ED.participantapplicationid And PJ.JobApplicationId = ED.JobApplicationId
Left Outer Join [MigArchive].[dbo].ParticipantJob PJa with(nolock) On
PJa.ParticipantApplicationId = ED.participantapplicationid And PJa.JobApplicationId = ED.JobApplicationId
Inner Join Job J with(nolock) On (J.JobId = PJ.JobId Or J.JobId = PJa.JobId)
left join ParticipantApplicationAssessmentDuration PAD with(nolock)
on (ed.ParticipantApplicationID=PAD.ParticipantApplicationID and ed.ParticipantID=PAD.ParticipantID AND ED.JobApplicationID=PAD.JobApplicationID)
--left join Simulations.dbo.SimulationDuration SD with(nolock) on (ed.ParticipantApplicationID=SD.ParticipantApplicationID AND ED.JobApplicationID=SD.JobApplicationID )
left join Assessment A with(nolock) on A.AssessmentID= PAD.AssessmentID 
left join AssessmentForm AF with(nolock) on(AF.AssessmentID=PAD.AssessmentID AND PAD.FormID=AF.FormID) 
where ed.ClientID in ('10365')--,'10365')
--AND ed.ProjectID = @ProjectID
--and CAST(ed.CreatedDate as date) >= @StartDate and CAST(ed.CreatedDate as date)<= @EndDate
and a.AssessmentID <> '992' and
CONVERT(VARCHAR(12),CAST(PAD.AssessmentDuration AS INT) /60/60 % 24) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) /60 % 60) + ':'
+ CONVERT(VARCHAR(2), CAST(PAD.AssessmentDuration AS INT) % 60) is not null 

UNION 

SELECT 
ParticipantApplicationID,
ParticipantID,
FirstName,LastName,
Email,
JobApplicationID,
JobApplicationName,
ClientID,
ClientLongName,
ProjectID,
ProjectName,
FormID,
FormName,
ProjectGroupID,
ProjectGroupName,
CreatedDate,
AssessmentID,
AssessmentName,
AssessmentStartTime,
AssessmentEndTime,
AssessmentDuration_Test,
CASE WHEN DATEDIFF(HH,AssessmentStartTime,AssessmentEndTime)/24 > 0 THEN
CONVERT(VARCHAR(12), CAST(AssessmentDuration_Test AS INT) /60/60/24) ELSE 0 END AS AssessmentDurationDays,
CONVERT(VARCHAR(12),CAST(AssessmentDuration_Test AS INT) /60/60 % 24) + ':'
+ CONVERT(VARCHAR(2), CAST(AssessmentDuration_Test AS INT) /60 % 60) + ':'
+ CONVERT(VARCHAR(2), CAST(AssessmentDuration_Test AS INT) % 60) AS AssessmentDuration
FROM (
select distinct
ed.ParticipantApplicationID,
ed.ParticipantID,
ed.FirstName,ed.LastName,
ed.Email,
ed.JobApplicationID,
ed.JobApplicationName,
ed.ClientID,
ed.ClientLongName,
ed.ProjectID,
ed.ProjectName,
AF.FormID,
AF.FormName,
ed.ProjectGroupID,
ed.ProjectGroupName,
ed.CreatedDate,
SD.AssessmentID,
A.AssessmentName,
SD.AssessmentStartTime,
SD.AssessmentEndTime,
ROUND(CAST(DATEDIFF(MILLISECOND,SD.AssessmentStartTime,SD.AssessmentEndTime) as float)/1000,2) AS AssessmentDuration_Test
From [dbo].[vwParticipantProgressExportDetails] As ED
Left Outer Join ParticipantJob  PJ with(nolock)
On PJ.ParticipantApplicationId = ED.participantapplicationid And PJ.JobApplicationId = ED.JobApplicationId
Left Outer Join [MigArchive].[dbo].ParticipantJob PJa with(nolock) On
PJa.ParticipantApplicationId = ED.participantapplicationid And PJa.JobApplicationId = ED.JobApplicationId
Inner Join Job J with(nolock) On (J.JobId = PJ.JobId Or J.JobId = PJa.JobId)
--left join ParticipantApplicationAssessmentDuration PAD with(nolock)
--on (ed.ParticipantApplicationID=PAD.ParticipantApplicationID and ed.ParticipantID=PAD.ParticipantID AND ED.JobApplicationID=PAD.JobApplicationID)
 join Simulations.dbo.SimulationDuration SD with(nolock) on (ed.ParticipantApplicationID=SD.ParticipantApplicationID AND ED.JobApplicationID=SD.JobApplicationID )
 join Assessment A with(nolock) on A.AssessmentID= SD.AssessmentID 
 join AssessmentForm AF with(nolock) on(AF.AssessmentID=SD.AssessmentID AND AF.FormID=SD.FormID) 
where ed.ClientID in ('10365','10363') 
) A
WHERE CONVERT(VARCHAR(12),CAST(AssessmentDuration_Test AS INT) /60/60 % 24) + ':'
+ CONVERT(VARCHAR(2), CAST(AssessmentDuration_Test AS INT) /60 % 60) + ':'
+ CONVERT(VARCHAR(2), CAST(AssessmentDuration_Test AS INT) % 60) IS NOT NULL 