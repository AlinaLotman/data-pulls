/*
CORE-6315 - Dec 2017 Sitel Data Pulls - profile recommendations (1) and testing time (2)
Parameters Passed - 
1. Client ID = 10303
2. Date Range - 1-Dec-2017 to 31-Dec-2017
3. Project ID = 
			--10988		Work @ Home
			--10989		Brick & Mortar
*/
select
ed.ParticipantApplicationID,ed.RecommendationLevel,
ed.RecommendationTitle,ed.ParticipantID,
ed.EmpID,ed.Email,
ed.FirstName,ed.LastName,
ed.ClientLongName,ed.ProjectName,
ed.City,ed.State,
ed.Zip,ed.Country,
ed.Phone1,ed.Phone2,
ed.EmployeeID,ed.StartDate,
ed.EndDate,ed.StreetAddress,
ed.JobApplicationName,ed.ProjectGroupName,
ed.JobApplicationID,j.JobID,
j.JobName,ed.CreatedDate,(Case
When PJ.RecommendationTitle Is Null Or PJ.RecommendationLevel = 2147483647
Then (Case
When PJa.RecommendationTitle Is Null Or PJa.RecommendationLevel = 2147483647
Then ''
Else PJa.RecommendationTitle
End)
Else PJ.RecommendationTitle
End) As RecoTitle,
(Case
When PJ.RecommendationTitle Is Null Or PJ.RecommendationLevel = 2147483647
Then (Case
When PJa.RecommendationTitle Is Null Or PJa.RecommendationLevel = 2147483647
Then ''
Else Cast(PJa.RecommendationLevel As varchar(5))
End)
Else Cast(PJ.RecommendationLevel As varchar(5))
End) As RecoLevel
From [dbo].[vwParticipantProgressExportDetails] As ED
Left Outer Join ParticipantJob PJ WITH(NOLOCK) On PJ.ParticipantApplicationId = ED.participantapplicationid And PJ.JobApplicationId = ED.JobApplicationId
Left Outer Join [MigArchive].[dbo].ParticipantJob PJa  WITH(NOLOCK) On PJa.ParticipantApplicationId = ED.participantapplicationid And PJa.JobApplicationId = ED.JobApplicationId
Inner Join Job J  WITH(NOLOCK) On (J.JobId = PJ.JobId Or J.JobId = PJa.JobId) 
where ed.ClientID = 10303 --Sitel 
and ed.ProjectID = 10989 --Brick & Mortar 
and CAST(ed.CreatedDate as date) between '2017-12-01' and '2017-12-31'
order by ed.createdDate